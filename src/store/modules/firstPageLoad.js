import axios from "axios";

const state = {
    firstPosts: [],
};

const getters = {
    perPagePosts: (state) => state.firstPosts,
};

const actions = {
    async fetchData({ commit }, page = 1) {
        // let page = 1;
        let data = [];
        let result = (
            await axios.get(
                `https://blog.ted.com/wp-json/wp/v2/posts?_fields=id,title,link,categories,content&page=${page}`
            )
        ).data;
        data = data.concat(result);
        commit("perPagePosts", data);
    },
};

const mutations = {
    perPagePosts: (state, firstPosts) => (state.firstPosts = firstPosts),
};

export default {
    state,
    getters,
    actions,
    mutations,
};
