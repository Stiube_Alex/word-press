import axios from "axios";

const state = {
    posts: [],
};

const getters = {
    allPosts: (state) => state.posts,
};

const actions = {
    async fetchData({ commit }) {
        let page = 1;
        let data = [];
        while (page <= 100) {
            let result = (
                await axios.get(
                    `https://blog.ted.com/wp-json/wp/v2/posts?_fields=id,title,link,categories,content&page=${page}`
                )
            ).data;
            if (result && result.length) {
                page++;
                data = data.concat(result);
            } else {
                break;
            }
        }
        commit("setPosts", data);
    },
};

const mutations = {
    setPosts: (state, posts) => (state.posts = posts),
};

export default {
    state,
    getters,
    actions,
    mutations,
};
