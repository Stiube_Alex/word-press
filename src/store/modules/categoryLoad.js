import axios from "axios";

const state = {
  categoryLoad: [],
};

const getters = {
  allData: (state) => state.categoryLoad,
};

const actions = {
  async fetchData({ commit }) {
    let page = 1;
    let data = [];
    while (true) {
      let result = (
        await axios.get(
          `https://blog.ted.com/wp-json/wp/v2/categories?_fields=name,id&page=${page}`
        )
      ).data;
      if (result && result.length) {
        page++;
        data = data.concat(result);
      } else {
        break;
      }
    }
    commit("setData", data);
  },
};

const mutations = {
  setData: (state, categoryLoad) => (state.categoryLoad = categoryLoad),
};

export default {
  state,
  getters,
  actions,
  mutations,
};
