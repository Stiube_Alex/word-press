// import { createStore } from "vuex";

import Vuex from "vuex";
// import Vue from "vue";
import categoryLoad from "./modules/categoryLoad";
import postsLoad from "./modules/postsLoad";
import firstPageLoad from "./modules/firstPageLoad";

// Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    categoryLoad,
    postsLoad,
    firstPageLoad
  },
});
